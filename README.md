# FirebaseConfig 
  
## Setup Project

Add this to your project build.gradle
Project-level build.gradle (<project>/build.gradle):

``` gradle
buildscript { 
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:3.2.1'

        classpath 'com.google.gms:google-services:4.2.0'

        // Add the Crashlytics Gradle plugin.
        classpath 'com.google.firebase:firebase-crashlytics-gradle:2.2.0'
 
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        maven {
            url 'https://maven.google.com/'
        }
        maven { url 'https://jitpack.io' } 
    }
}
```




Add this to your project build.gradle
Module-level build.gradle (<module>/build.gradle): 

[![](https://jitpack.io/v/org.bitbucket.droidhelios/firebase-config.svg)](https://jitpack.io/#org.bitbucket.droidhelios/firebase-config)
```gradle 
// Apply the Crashlytics Gradle plugin
apply plugin: 'com.google.firebase.crashlytics'

dependencies {
    implementation 'org.bitbucket.droidhelios:firebase-config:x.y'

    // Add the Firebase Crashlytics SDK.
    implementation 'com.google.firebase:firebase-crashlytics:17.1.0'
}

// ADD THIS AT THE BOTTOM
apply plugin: 'com.google.gms.google-services'
```


## Setup Firebase
```gradle
Add Remote Config Parameter
1.  Key    : isAppLive
    Value  : true
2.  Key    : message
    Value  : Trial period is over. Please contact to IT Team.
```

 
#### Usage method
```java
public class MainActivity extends BaseRemoteConfigActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    } 
    
    public void onButtonClicked(View view) {
        if(RemoteConfig.isAppNotExpired(this)){
            Toast.makeText(this, "Valid click", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this, RemoteConfig.getAppExpiredErrorMessage(this), Toast.LENGTH_SHORT).show();
        }
    }
}
```

#### Required Base Activity
```java
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.firebaseconfig.RemoteConfig;

public abstract class BaseRemoteConfigActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        RemoteConfig.newInstance(this)
                .fetch(new RemoteConfig.RemoteCallback() {
                    @Override
                    public void onValidUser() {
                    }

                    @Override
                    public void onInvalidUser(String message) {
                        showErrorMessage(message);
                    }

                    @Override
                    public void onError(String message) {

                    }
                });
    }

    private void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
```



#### Useful Links:
1. https://firebase.google.com/docs/android/setup
2. https://firebase.google.com/docs/crashlytics/get-started