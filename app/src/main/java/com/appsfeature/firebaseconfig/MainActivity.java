package com.appsfeature.firebaseconfig;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.firebaseconfig.RemoteConfig;

public class MainActivity extends BaseRemoteConfigActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        (findViewById(R.id.btnAction)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               takAction();
            }
        });
    }

    private void takAction() {
        if (RemoteConfig.isAppExpired(MainActivity.this)) {
            Toast.makeText(this, RemoteConfig.getAppExpiredErrorMessage(this), Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(this, "Valid click", Toast.LENGTH_SHORT).show();
    }
}
